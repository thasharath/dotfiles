if status is-interactive

    # Commands to run in interactive sessions can go here
end

source ~/.scripts/alias
set -U fish_greeting
starship init fish | source
zoxide init fish | source

