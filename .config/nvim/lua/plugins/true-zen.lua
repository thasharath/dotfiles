return {
  'Pocco81/true-zen.nvim',

  config = function()
    local status, true_zen = pcall(require, "true-zen")
    if not status then
      return
    end

    true_zen.setup({
      modes = {
        ataraxis = {
          callbacks = {
            open_pre = function()
              require("lualine").hide()
            end,
            close_pre = function()
              require("lualine").hide({ unhide = true })
            end,
          },
        },
      },
    })


    vim.api.nvim_set_keymap("n", "<leader>zn", ":TZNarrow<CR>", {})
    vim.api.nvim_set_keymap("v", "<leader>zn", ":'<,'>TZNarrow<CR>", {})
    vim.api.nvim_set_keymap("n", "<leader>zf", ":TZFocus<CR>", {})
    vim.api.nvim_set_keymap("n", "<leader>zm", ":TZMinimalist<CR>", {})
    vim.api.nvim_set_keymap("n", "<leader>za", ":TZAtaraxis<CR>", {})
  end,

}
