expand-or-complete-with-dots() {
echo -ne "\033[0;31m....\033[0m"
zle expand-or-complete
zle redisplay
}
zle -N expand-or-complete-with-dots
bindkey "^I" expand-or-complete-with-dots
