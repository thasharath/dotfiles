#!/bin/bash

# Check if ffmpeg is installed
if ! command -v ffmpeg &> /dev/null; then
    echo "ffmpeg is not installed. Please install ffmpeg first."
    exit 1
fi

# Check if an argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <input_directory>"
    exit 1
fi

input_dir="$1"
output_dir="./output_m4a"

# Create the output directory if it doesn't exist
mkdir -p "$output_dir"

# Convert each MP3 file to M4A with AAC codec and 256kbps bitrate
for file in "$input_dir"/*.mp3; do
    if [ -e "$file" ]; then
        filename=$(basename "$file")
        filename_no_extension="${filename%.*}"
        output_file="$output_dir/$filename_no_extension.m4a"
        ffmpeg -i "$file" -vn -codec:a aac -b:a 256k "$output_file"
        echo "Converted $filename to M4A with AAC codec and 256kbps bitrate"
    else
        echo "No MP3 files found in $input_dir"
        exit 1
    fi
done

echo "Conversion complete."
