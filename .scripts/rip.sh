#!/bin/bash

# Check if zip is installed
if ! command -v zip &> /dev/null
then
    echo "zip could not be found. Please install it and try again."
    exit 1
fi

# Check if telegram-upload is installed
if ! command -v telegram-upload &> /dev/null
then
    echo "telegram-upload could not be found. Please install it and try again."
    exit 1
fi

# Check if at least one URL is provided
if [ $# -eq 0 ]
then
    echo "No URL provided. Usage: ./rip.sh <url1> <url2> ..."
    exit 1
fi

# Loop over each argument (URL)
for url in "$@"
do
    # Use rip to download content
    rip --no-db url "$url"

    # Change to the directory where the downloaded folders are stored
    cd ~/StreamripDownloads/

    # Loop over each folder in the current directory
    for dir in */
    do
        # Remove trailing slash
        base=$(basename "$dir")
        # Compress folder to a zip file
        zip -r "${base}.zip" "$dir"
    done

    # Loop over each zip file in the current directory
    for zipfile in *.zip
    do
        # Upload zip file to Telegram
        telegram-upload --to https://t.me/+Gcc-_RwHps5hYTg1 --sort --directories recursive --large-files split --force-file "$zipfile"
    done

   # Delete the StreamripDownloads directory and its contents
    cd ~
    rm -rfv ~/StreamripDownloads/*
done