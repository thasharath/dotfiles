# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$PATH:$HOME/.local/bin

export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000

# Export Env Variables
export EDITOR=nvim
export BROWSER=firefox
export VISUAL=nvim
export HISTCONTROL=ignoreboth
export TERMINAL=kitty

export FZF_DEFAULT_OPTS="
--color=fg:#908caa,bg:#191724,hl:#ebbcba
--color=fg+:#e0def4,bg+:#26233a,hl+:#ebbcba
--color=border:#403d52,header:#31748f,gutter:#191724
--color=spinner:#f6c177,info:#9ccfd8,separator:#403d52
--color=pointer:#c4a7e7,marker:#eb6f92,prompt:#908caa"

zstyle ':completion:*:*:git:*' menu select script /usr/local/etc/bash_completion.d/git-completion.bash
fpath=(/usr/local/share/zsh-completions $fpath)
autoload -U compinit && compinit
zmodload -i zsh/complist
setopt autocd extendedglob nomatch notify appendhistory
# unsetopt beep
bindkey -e
ENABLE_CORRECTION="true"

#PROMPT=$'\n%B%F{73} %c%f%b\n%B%(?.%{%F{green}%}.%F{red})%(!.#.λ)%f%b '
# PROMPT=$'\n%B%F{cyan}%n%f%b%B at %b%B%F{cyan}%M%f%b%B in %b%B%F{yellow}%f%b\n%B%(?.%{%F{green}%}.%F{red})%(!.#.λ)%f%b '

# Source Plugins
source ~/.zsh/plugins/extract/extract.plugin.zsh
source ~/.zsh/plugins/colored-man-pages/colored-man-pages.plugin.zsh
source ~/.zsh/plugins/command-not-found/command-not-found.plugin.zsh
source ~/.zsh/plugins/completion.zsh
source ~/.zsh/plugins/correction.zsh
source ~/.zsh/plugins/dnf/dnf.plugin.zsh
source ~/.zsh/plugins/key-bindings.zsh
source ~/.zsh/plugins/safe-paste/safe-paste.plugin.zsh
source ~/.zsh/plugins/spectrum.zsh
source ~/.zsh/plugins/systemd/systemd.plugin.zsh
source ~/.zsh/plugins/theme-and-appearance.zsh
source ~/.zsh/plugins/waiting.plugin.zsh
source ~/.zsh/plugins/termsupport.zsh

source ~/.zsh/catppuccin_macchiato-zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

source ~/.scripts/alias

# Keybindings
bindkey '^[[P'	delete-char		#delete
bindkey '^[[H'	beginning-of-line	#home
bindkey '^[[F'	end-of-line		#end

# ~/.scripts/motiv/motiv
# ~/.scripts/tmux-default

eval "$(starship init zsh)"
